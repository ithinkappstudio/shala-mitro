<?php

namespace App\Model;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Book
 * @package App\Model\Admin
 * @version October 26, 2020, 3:50 pm UTC
 *
 * @property Collection $books
 * @property string $name
 * @property string $seller_name
 * @property string $book_pdf
 * @property string $language
 * @property integer $book_category_id
 */
class Book extends Model
{

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'             => 'required|unique:books',
        'seller_name'      => 'required',
        'book_pdf'         => 'required|mimes:pdf',
        'language'         => 'required',
        'semester'         => 'required',
        'book_category_id' => 'required',
    ];
    public $table = 'books';

    public $fillable = [
        'name',
        'seller_name',
        'book_pdf',
        'language',
        'semester',
        'book_category_id',
    ];

    const SEMESTER = [
        1 => 'Semester 1',
        2 => 'Semester 2',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'               => 'integer',
        'name'             => 'string',
        'seller_name'      => 'string',
        'book_pdf'         => 'string',
        'language'         => 'string',
        'semester'         => 'string',
        'book_category_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(BookCategory::class, 'book_category_id');
    }
}
