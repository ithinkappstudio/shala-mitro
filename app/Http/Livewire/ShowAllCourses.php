<?php

namespace App\Http\Livewire;

use App\Model\Book;
use App\Model\Category;
use App\Model\OldPaper;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ShowAllCourses extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $pagination = 6;
    public $filter = 'Books';
    public $search = '';

    public function updatingPagination()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }


    public function render()
    {
        if ($this->filter == 'Books') {
            $query = Book::with('category');
        }
        if ($this->filter == 'Papers') {
            $query = OldPaper::with('category');
        }
        if ($this->filter == 'QuizCategory') {
            $query = Category::with('question')->withCount('question');
        }
        $query->when($this->search != '', function (Builder $q) {
            $q->where('name', 'like', '%'.$this->search.'%');
        });
        $courses = $query->paginate($this->pagination);

        return view('livewire.show-all-courses', compact('courses'));
    }
}
