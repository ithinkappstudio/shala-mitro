<?php

namespace App\Http\Livewire;

use App\Model\BookCategory;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ShowBooks extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $allbookCategories;
    public $categoryId = '';

    public function mount()
    {
        $this->allbookCategories = BookCategory::has('books')->get();
    }

    public function updatingcategoryId()
    {
        $this->resetPage();
    }

    public function render()
    {
        $bookCategories = BookCategory::has('books')
            ->when(! empty($this->categoryId), function (Builder $query) {
                $query->findOrFail($this->categoryId);
            })->paginate(10);

        return view('livewire.show-books', compact('bookCategories'));
    }
}
