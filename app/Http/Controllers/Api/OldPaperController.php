<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Book;
use App\Model\OldPaper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class OldPaperController extends Controller
{
    /**
     * @param  Request  $request
     * 
     * @return JsonResponse
     */
    public function allOldPapers(Request $request)
    {
        $data = ['success' => true, 'message' => __('all old papers retrieved Went wrong !')];
        $categoryId = $request->get('category');
        $semester = $request->get('semester');
        $medium = $request->get('medium');
        
        $books = OldPaper::with('category');
        $books->when($categoryId,function (Builder $query) use  ($categoryId){
            $query->where('paper_category_id',$categoryId);
        });
        $books->when($semester,function (Builder $query) use  ($semester){
            $query->where('semester',$semester);
        });
        $books->when($medium,function (Builder $query) use  ($medium){
            $query->where('language',$medium);
        });
        
        $data['data']['old_papers'] = $books->get();

        return response()->json($data);
    }
    
}
