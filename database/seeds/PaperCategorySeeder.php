<?php

use App\Model\PaperCategory;
use App\Model\OldPaper;
use Illuminate\Database\Seeder;

class PaperCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach (range(1, 10) as $index) {
            $input = [
                'name'   => 'ધોરણ '.$index,
                'status' => 1,
            ];
            $paperCategory = PaperCategory::create($input);
            $input = [
                [
                    'name'              => 'અંગ્રેજી',
                    'creator_name'      => 'ગાલા',
                    'language'          => 'Gujarati',
                    'semester'          => 1,
                    'paper_category_id' => $paperCategory->id,
                ],
                [
                    'name'              => 'હિન્દી',
                    'creator_name'      => 'ગાલા',
                    'language'          => 'English',
                    'semester'          => 1,
                    'paper_category_id' => $paperCategory->id,
                ],
                [
                    'name'              => 'ગણિત',
                    'creator_name'      => 'ગાલા',
                    'language'          => 'Gujarati',
                    'semester'          => 2,
                    'paper_category_id' => $paperCategory->id,
                ],
                [
                    'name'              => 'વિજ્ઞાન',
                    'creator_name'      => 'ગાલા',
                    'language'          => 'Gujarati',
                    'semester'          => 2,
                    'paper_category_id' => $paperCategory->id,
                ],
            ];

            foreach ($input as $data) {
                OldPaper::create($data);
            }
        }
    }
}
