<!-- Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('id', 'Id:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->id }}</p>
</div>


<!-- Name Field -->
<div class="form-group col-md-6">
    {!! Form::label('name', 'Name:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->name }}</p>
</div>


<!-- Email Field -->
<div class="form-group col-md-6">
    {!! Form::label('email', 'Email:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->email }}</p>
</div>


<!-- Message Field -->
<div class="form-group col-md-6">
    {!! Form::label('message', 'Message:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->message }}</p>
</div>


<!-- Created At Field -->
<div class="form-group col-md-6">
    {!! Form::label('created_at', 'Created At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->created_at }}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-md-6">
    {!! Form::label('updated_at', 'Updated At:',['class' => 'font-weight-bold']) !!}
    <p>{{ $faqs->updated_at }}</p>
</div>


