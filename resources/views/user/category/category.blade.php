@extends('user.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection

@section('main-body')
<!-- category-area start-->
<div class="category-area">
    <div class="container mt-5">
        <div class="category-wrap">
            @include('layout.message')
            <div class="row">
                <div class="col-lg-3">
                    <div class="category-tab-list">
                        <ul class="nav nav-tabs">
                            @if(isset($categories[0]))
                                @foreach($categories as $item)
                                    <li>
                                        <a @if($category->id == $item->id) class="active" @endif
                                            @if(check_category_unlock($item->id, $item->coin) == 1) onclick='open_modal("{{$item->id}}","{{$item->coin}}","{{$item->name}}");'
                                            @else href="{{route('categoryData', encrypt($item->id))}}" @endif >
                                            <img @if(!empty($item->image)) src="{{asset(path_category_image().$item->image)}}" @else
                                            src="{{asset('assets/user/images/category/img-6.png')}}" @endif  alt="">
                                            @if(check_category_unlock($item->id, $item->coin) == 1)
                                                <span class="pull-right">
                                                    <img src="{{asset('assets/user/images/category/img-10.png')}}" alt="">
                                                </span>
                                            @endif
                                            {{$item->name}} ({{count_question($item->id)}})
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                            <li><a class="br-b"></a></li>
                            <li><a class="br-b"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="tab-content">
                        <div id="Science" class="tab-pane active">
                            <div class="category-sub-area category-sub-area-p">
                                @if(isset($subCategories[0]))
                                    <div class="row">

                                        @foreach($subCategories as $item)
                                            @include('user.category.category_list')
                                        @endforeach
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <p class="text-danger text-center">{{__('No data found')}}</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- category-area end-->
@include('user.category.unlock_category_model')
@endsection

@section('script')
    <script>
        function open_modal(id, coin, name)
        {
            $('#UnlockModal').modal('show');
            $('.cat-name').text(name);
            $('#cat-coin').text(coin);
            $('#cat-id').val(id);
        };
    </script>
@endsection
