<div class="col-lg-3 col-sm-6 col-12">
    @if(check_category_unlock($item->id, $item->coin) == 1)
        <a onclick='open_modal("{{$item->id}}","{{$item->coin}}","{{$item->name}}");'>
            @else
                <a href="{{ route('categoryData', encrypt($item->id))}}">
                    @endif
                    <div class="card-hover-border trans_400 category-single">
                        <div class="category-img">
                            <img src="{{ (!empty($item->white_image)) ? asset(path_category_image().$item->white_image) : asset('web_assets/images/icon_3.png') }}"
                                 alt="">
                        </div>
                        <div class="category-text ">
                            <h2>{{$item->name}}</h2>
                            <ul class="mt-3 text-left ml-2">
                                <li class="d-flex justify-content-between">
                                    <div>{{__('Total Question')}}:</div>
                                    <div>{{count_question($item->id)}}</div>
                                </li>
                                {{--                <li class="d-flex justify-content-between">--}}
                                {{--                    <div>Times:</div>--}}
                                {{--                    <div>{{count_question($item->id)}}</div>--}}
                                {{--                </li>--}}
                                {{--                <li class="d-flex justify-content-between">--}}
                                {{--                    <div>{{__('Total Point')}}</div>--}}
                                {{--                    <div>{{count_question($item->id)}}</div>--}}
                                {{--                </li>--}}
                            </ul>
                        </div>
                        <a href="{{ route('categoryData', encrypt($item->id))}}"
                           class="btn btn-outline-primary btn-sm mt-4 text-uppercase">
                            Start Quiz
                        </a>

                        {{--        @if(check_category_unlock($item->id, $item->coin) == 1)--}}
                        {{--            <div class="lock border-danger">--}}
                        {{--                <span>--}}
                        {{--                    <i class="fas fa-lock text-danger"></i>--}}
                        {{--                </span>--}}
                        {{--            </div>--}}
                        {{--        @else--}}
                        {{--            <div class="lock border-success">--}}
                        {{--                <span>--}}
                        {{--                    <i class="fas fa-lock-open text-success"></i>--}}
                        {{--                </span>--}}
                        {{--            </div>--}}
                        {{--        @endif--}}
                    </div>
                </a>
</div>
