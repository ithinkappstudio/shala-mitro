@extends('user.master')
@section('title') @if (isset($pageTitle)) {{ $pageTitle }} @endif @endsection
@if(session()->has('question_list.0'))
    @php $qId = session('question_list.0.id'); @endphp
    @php $index = 0; @endphp
@endif
@section('main-body')
    <!-- Chemistry area start-->
    <div class="container bg-white  shadow pt-3 mb-4">
        <div class="d-flex justify-content-end">
            <a class="btn btn-outline-primary" href="{{route('userDashboardView')}}">{{__('Back')}}</a>
        </div>
        <div class="row mt-4 justify-content-center">
            <div class="col-lg-6 col-sm-12">
                <div class="chemistry-item">
                    <h2>{{__('In')}} {{$category->name}} {{__('Category')}}</h2>
                    <ul>
                        <li>
                            <span>
                                {{__('Total Question')}} :
                                <i class="far fa-question-circle"></i>
                            </span>
                            <span>
                                {{isset($total_question) ? $total_question : 0}}
                            </span>
                        </li>
                        <li>
                            <span>
                                {{__('Total Point')}} :
                                <i class="far fa-money-bill-alt"></i>
                            </span>
                            <span>
                                {{isset($total_point) ? $total_point : 0}}
                            </span>
                        </li>
                        <li>
                            <span>
                            {{__('Total Coin')}} :
                            <i class="fas fa-coins"></i>    
                            </span>
                            <span>
                                {{isset($total_coin) ? $total_coin : 0}}
                            </span>
                        </li>
                    </ul>
                </div>
                <div class="unlock-btn my-4">
                    @if(isset($index))
                        <a href="{{route('singleQuestion',[$index,$qId])}}">
                            <button type="button">{{__("Let’s Start")}}</button>
                        </a>
                    @endif
                    <a href="{{route('userDashboardView')}}">
                        <button type="button" class="btn-cl">{{__('Cancel')}}</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Chemistry area end-->
@endsection

@section('script')
@endsection
