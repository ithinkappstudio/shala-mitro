<!-- Footer -->
@php
    $settings = allsetting();
@endphp
<footer class="footer">
    <div class="footer_background"
         style="background-image:url({{ asset('web_assets/images/footer_background.png') }})"></div>
    <div class="container">
        <div class="row footer_row">
            <div class="col">
                <div class="footer_content">
                    <div class="row">

                        <div class="col-lg-3 footer_col">

                            <!-- Footer About -->
                            <div class="footer_section footer_about">
                                <div class="footer_logo_container">
                                    <a href="#">
                                        <div class="footer_logo_text"> @if(getAppName())
                                                {!!  substr(getAppName(),0,-2) . '<span>'. substr(getAppName(),-2,2).'</span>' !!}

                                            @else
                                                Qu<span>iz</span>
                                            @endif</div>
                                    </a>
                                </div>
                                <div class="footer_about_text">

                                    <p>@if($settings['landing_footer_des']){{ $settings['landing_footer_des'] }}@else
                                            Lorem ipsum dolor sit ametium, consectetur adipiscing elit.
                                        @endif
                                    </p>
                                </div>
                                <div class="footer_social">
                                    <ul>
                                        <li><a href="{{ $settings['facebook_url'] }}"><i class="fab fa-facebook ml-0"
                                                                                         aria-hidden="true"></i></a>
                                        </li>
                                        <li><a href="{{ $settings['google_url'] }}"><i class="fab fa-google-plus ml-0"
                                                                                       aria-hidden="true"></i></a>
                                        </li>
                                        <li><a href="{{ $settings['instagram_url'] }}"><i class="fab fa-instagram ml-0"
                                                                                          aria-hidden="true"></i></a>
                                        </li>
                                        <li><a href="{{ $settings['twitter_url'] }}"><i class="fab fa-twitter ml-0"
                                                                                        aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer Contact -->
                            <div class="footer_section footer_contact">
                                <div class="footer_title">Contact Us</div>
                                <div class="footer_contact_info">
                                    <ul>
                                        <li>Email: @if($settings['primary_email']){{ $settings['primary_email'] }}@else
                                                Info.deercreative@gmail.com
                                            @endif</li>
                                        <li>Phone:
                                            @if($settings['app_phone']){{ $settings['app_phone'] }}@else
                                                +(88) 111 555 666
                                            @endif</li>
                                        <li>
                                            @if($settings['landing_footer_address'])
                                                {{ $settings['landing_footer_address'] }}
                                            @else
                                                40 Baria Sreet 133/2 New York City, United States
                                            @endif</li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col">

                            <!-- Footer links -->
                            <div class="footer_section footer_links">
                                <div class="footer_title">Contact Us</div>
                                <div class="footer_links_container">
                                    <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about.blade.php">About</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li><a href="#">Features</a></li>
                                        <li><a href="courses.html">Courses</a></li>
                                        <li><a href="#">Events</a></li>
                                        <li><a href="#">Gallery</a></li>
                                        <li><a href="#">FAQs</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-3 footer_col clearfix">

                            <!-- Footer links -->
                            <div class="footer_section footer_mobile">
                                <div class="footer_title">Mobile</div>
                                <div class="footer_mobile_content">
                                    <div class="footer_image"><a href="#"><img
                                                    src="{{ asset('web_assets/images/mobile_1.png') }}" alt=""></a>
                                    </div>
                                    <div class="footer_image"><a href="#"><img
                                                    src="{{ asset('web_assets/images/mobile_2.png') }}" alt=""></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row copyright_row">
            <div class="col">
                <div class="copyright d-flex flex-lg-row flex-column align-items-center justify-content-start">
                    <div class="cr_text">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                            aria-hidden="true"></i> by <a
                                href="#" target="_blank">Sala Mitro</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
                    <div class="ml-lg-auto cr_links">
                        <ul class="cr_list">
                            <li><a href="#">Copyright notification</a></li>
                            <li><a href="{{route('termsCondition')}}">Terms of Use</a></li>
                            <li><a href="{{ route('privacyPolicy') }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
